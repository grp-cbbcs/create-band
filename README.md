 # Bioimage ANalysis Desktop (BAND) #
 The [BAND](https://band.embl.de) is a cloud based virtual desktop service accessible with a web browser so that you can work on your data from anywhere with an internet connection.

## Installation  ## 
  Please refer to the [WIKI page](https://git.embl.de/ysun/create-band/-/wikis/Home) for setting up BAND


The code is partly based on https://github.com/monash-merc/ansible_cluster_in_a_box


## License  ## 
[GPL](https://git.embl.de/ysun/create-band/-/raw/main/LICENSE)
